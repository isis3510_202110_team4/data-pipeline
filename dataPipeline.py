import csv
import requests

URL = 'http://ec2-52-90-6-21.compute-1.amazonaws.com/api/'

clientList = 'users/client'

workerList = 'users/worker'

with requests.Session() as s:
    download = s.get(URL+clientList)
    download2 = s.get(URL+workerList)
    client = download.json()
    worker = download2.json()
    idsClient = []

def write_to_file(i, array,subroute):
    with requests.Session() as s:
        if i == '':
            f = open('breve.csv','a')
        else:
            f = open(i+'.csv','a')
        csv_writer = csv.writer(f)
        count = 0
        for j in array:
            download = s.get(URL+subroute+str(j)+'/'+i)
            try:
                download = download.json()
                if len(download)==0:
                    continue
            except Exception as e:
                print(e)
                print(URL+subroute+str(j)+'/'+i)
                continue
            
            if isinstance(download,list):
                download = download[0]
            if count ==0:
                count+=1
                headers = download.keys()
                csv_writer.writerow(headers)
            csv_writer.writerow(download.values())
        f.close()

def write_to_file_simple(i, subroute):
    with requests.Session() as s:
        if i == '':
            f = open('breve.csv','a')
        else:
            f = open(i+'.csv','a')
        csv_writer = csv.writer(f)
        
        download = s.get(URL+subroute+'/'+i)
        try:
            download = download.json()
            if len(download)==0:
                return
        except Exception as e:
            print(e)
            print(URL+subroute+'/'+i)
            return
        if isinstance(download,list):
            download = download[0]
        if isinstance(download,dict):
            headers = download.keys()
            csv_writer.writerow(headers)
            csv_writer.writerow(download.values())
        f.close()

for i in client:
    idsClient.append(i['id'])

others = ['favorite_workers','request_frequency', 'favorite_category']

for i in others:
    write_to_file(i,idsClient,clientList+'/detail/')
idsWorker = []
for i in worker:
    idsWorker.append(i['id'])

write_to_file('',idsClient,'users/worker/closest/')

others = ['num_favorites','services_completed']
for i in others:
    write_to_file(i,idsWorker,workerList+'/detail/')




others = ['best_by_category','best_by_category_month','worst_by_category','completed_per_worker']

for i in others:
    write_to_file_simple(i,workerList)

category = "category"

others = ['most_requested','average_service_radius','least_requested','avgcost','least_registered','workers_per_category']

for i in others:
    write_to_file_simple(i,category)


    

def write_to_file_service():
    with requests.Session() as s:
        f = open('service.csv','a')
        csv_writer = csv.writer(f)
        
        download = s.get(URL+"service"+'/')
        try:
            download = download.json()
            if len(download)==0:
                return
        except Exception as e:
            print(e)
            print(URL+'service'+'/')
            return
        if isinstance(download,list):
            download = download[0]
        if isinstance(download,dict):
            headers = download.keys()
            csv_writer.writerow(headers)
            csv_writer.writerow(download.values())
        f.close()

write_to_file_service()